#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <pthread.h>
#include <regex.h>

#define TESTING

#ifdef TESTING
#define test main

int begin(int argc, char **argv);

#else
#define begin main
#endif /* TESTING */

#define PROGRAM "../program" // TODO at the first moment, these will remain hardcoded
#define MEMORY "../memory"
#define PATH_SIZE 1024

volatile sig_atomic_t sigterm = 0;

static pthread_t tClockId;
static FILE *fProgram;
static FILE *fMemory;

/** registers **/
static char *regIR, *regMBR;
static int regMAR = 0, regPC = 1;
static int regA, regB, regC, regD, regE;

/********************************************************/
/** PROTOTYPES                                         **/
/********************************************************/
int main(int argc, char **argv);

void term(int signum);

void *thClock(void *vargp);

void remLineBreakFromMBR();

#pragma clang diagnostic push
#pragma ide diagnostic ignored "OCUnusedGlobalDeclarationInspection"
void __getfilename__(FILE *file, char *result);
#pragma clang diagnostic pop

/********************************************************/
/** ENTRY POINT                                        **/
/********************************************************/
int test(int argc, char **argv) {
//    // test to find out about the execution path behaviour in Ubuntu 16.04
//    FILE *fProgram = fopen(".", "r");
//    char result[PATH_SIZE];
//    __getfilename__(fProgram, result);
//    printf("file name: %s", result);
//    fclose(fProgram);
//    exit(EXIT_SUCCESS);

    // execution test
    return begin(argc, argv);
}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-parameter"
int begin(int argc, char **argv) {
    // setup SIGTERM handler
    struct sigaction action;
    memset(&action, 0, sizeof(action));
    action.sa_handler = term;
    sigaction(SIGTERM, &action, NULL);

    // ***
    // START CONTROL UNIT MODULES
    // ***
    // start bus modules
    printf("Loading PROGRAM file\n");
    fProgram = fopen(PROGRAM, "r");
    if (fProgram == NULL) {
        printf("Error opening file PROGRAM (reason: %s)\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    printf("Loading MEMORY file\n");
    fMemory = fopen(MEMORY, "r+");
    if (fMemory == NULL) {
        printf("Error opening file MEMORY (reason: %s)\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    // startup clock module and wait
    pthread_create(&tClockId, NULL, thClock, NULL);
    pthread_join(tClockId, NULL);

    exit(EXIT_SUCCESS);
}
#pragma clang diagnostic pop

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-parameter"
void *thClock(void *vargp) {
    int cycle = 0;
    ssize_t read = 0;
    while (!sigterm && read != -1) {
        cycle++;
        printf("╔════════════════════════════════════════════════╗\n");
        printf("║                                                ║\n");
        printf("║                CLOCK CYCLE N %03d               ║\n", cycle);
        printf("║                                                ║\n");
        printf("╠════════════════════════════════════════════════╣\n");
        printf("║ FETCH:                                         ║\n");
        printf("╠════════════════════════════════════════════════╣\n");

        // fetch step 1
        regMAR = regPC;
        char strfetch1[36];
        sprintf(strfetch1, "MAR <- PC(%010d)", regMAR);
        printf("║ %46s ║\n", strfetch1);

        // fetch step 2
        size_t len = 0;
        rewind(fProgram);
        int linenum = 0;
        for(; (read = getline(&regMBR, &len, fProgram)) != -1 && linenum != regMAR; ++linenum);
        remLineBreakFromMBR();
        // print fetch step 2
        char strfetch2[36];
        sprintf(strfetch2, "MBR <- Memory(%010d)", regMAR);
        printf("║ %46s ║\n", strfetch2);

        // fetch step 3
        char strfetch3[36];
        sprintf(strfetch3, "PC <- PC(%010d) + 1", regPC++);
        printf("║ %46s ║\n", strfetch3);

        regIR = regMBR;
        // fetch step 4
        char strfetch4[36];
        sprintf(strfetch4, "IR <- MBR(%s)", regMBR);
        printf("║ %46s ║\n", strfetch4);

        printf("╠════════════════════════════════════════════════╣\n");
        printf("║ EXECUTE:                                       ║\n");
        printf("╠════════════════════════════════════════════════╣\n");

        // execute step 1
        regex_t regex;
        regmatch_t regmatch[3];
        if(regcomp(&regex, "^([[:alpha:]]+)[[:space:]](.+)$", REG_EXTENDED)) {
            printf("Error compiling regular expression!");
            exit(EXIT_FAILURE);
        }
        if(regexec(&regex, regIR, 3, regmatch, 0)) {
            char strexecerr[36];
            sprintf(strexecerr, "UNRECOGNIZED INSTRUCTION (%s)", regIR);
            printf("║ %46s ║\n", strexecerr);
            printf("╚════════════════════════════════════════════════╝\n");
            break; // exit the while loop in order to finalize the program execution
        } else {
            regfree(&regex);
            // retrieve opcode
            char opccpy[strlen(regIR) + 1];
            strcpy(opccpy, regIR);
            opccpy[regmatch[1].rm_eo] = 0;
            char *opcode = opccpy + regmatch[1].rm_so;
            char sOpcode[36];
            sprintf(sOpcode, "%-s:", opcode);
            printf("║ %46s ║\n", sOpcode);

            // process execution as for the opcode
            size_t szOpcode = strlen(opcode);
            if(strncmp(opcode, "MOV", szOpcode) == 0) {
                // retrieve operands
                char opecpy[strlen(regIR) + 1];
                strcpy(opecpy, regIR);
                opecpy[regmatch[2].rm_eo] = 0;
                char *operands = opecpy + regmatch[2].rm_so;
                // fetch first operand
                regex_t op1rgx;
                regmatch_t opregmatch;
                if(regcomp(&regex, "^([[:alnum:]]+),[[:space:]]?([[:alnum::]]+)$", REG_EXTENDED)) {
                    printf("Error compiling regular expression!");
                    exit(EXIT_FAILURE);
                }
                if(regexec(&op1rgx, operands, 3, &opregmatch, 0)) {
                    char strexecerr[36];
                    sprintf(strexecerr, "MISSING FIRST OPERANDS (%s)", regIR);
                    printf("║ %46s ║\n", strexecerr);
                    printf("╚════════════════════════════════════════════════╝\n");
                    break; // exit the while loop in order to finalize the program execution
                }
                // retrieve first operand
                char strdst[strlen(operands) + 1];
                strcpy(strdst, operands);
                strdst[opregmatch.rm_eo] = 0;
                char *opdst = strdst + opregmatch.rm_so;
                // criticize first operand
                if(regcomp(&regex, "^([[:digit:]]{1,2}))$", REG_EXTENDED)) {
                    printf("Error compiling regular expression!");
                    exit(EXIT_FAILURE);
                }
                if(*opdst == 'L') { // destination is memory
                    opdst++;
                    regMAR = atoi()
                } else { // destination is a named register

                }

                // retrieve second operand
                char strsrc[strlen(operands) + 2];
                strcpy(strsrc, operands);
                strsrc[opregmatch.rm_eo] = 0;
                char *opsrc = strsrc + opregmatch.rm_so;

            } else if(strncmp(opcode, "ADD", szOpcode) == 0) {
                // TODO
            } else if(strncmp(opcode, "SUB", szOpcode) == 0) {
                // TODO
            } else if(strncmp(opcode, "MUL", szOpcode) == 0) {
                // TODO
            } else if(strncmp(opcode, "DIV", szOpcode) == 0) {
                // TODO
            } else if(strncmp(opcode, "AND", szOpcode) == 0) {
                // TODO
            } else if(strncmp(opcode, "OR", szOpcode) == 0) {
                // TODO
            } else if(strncmp(opcode, "JMP", szOpcode) == 0) {
                // TODO
            } else if(strncmp(opcode, "JE", szOpcode) == 0) {
                // TODO
            } else if(strncmp(opcode, "JZ", szOpcode) == 0) {
                // TODO
            }
        }

        printf("╚════════════════════════════════════════════════╝\n");
        printf("\n");
        if (regIR) {
            free(regIR);
        }

        sleep(1); // TODO replace with nanosleep in order to simulate increasing the clock frequency
        printf("\n");
    }
    printf("╔════════════════════════════════════════════════╗\n");
    printf("║ EOP                                            ║\n");
    printf("╚════════════════════════════════════════════════╝\n");

    fclose(fProgram);
}
#pragma clang diagnostic pop

/** Remove the line break at the end of the line for better console formatting **/
void remLineBreakFromMBR() {
    char *aux = regMBR;
    while (*aux != (char) '\0') {
        if (*aux == '\n')
            *aux = (char) '\0';
        aux++;
    }
}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-parameter"
void term(int signum) {
    sigterm = 1;
}
#pragma clang diagnostic pop

/********************************************************/
/** TEST FUNCTIONS ----------------------------------- **/
#pragma clang diagnostic push
#pragma ide diagnostic ignored "OCUnusedGlobalDeclarationInspection"
/********************************************************/
void __getfilename__(FILE *file, char *result) {
    char path[PATH_SIZE];
    int fd = fileno(file);
    /* Read out the link to our file descriptor. */
    sprintf(path, "/proc/self/fd/%d", fd);
    memset(result, 0, PATH_SIZE);
    readlink(path, result, PATH_SIZE - 1);
}
#pragma clang diagnostic pop

